

namespace ClinicaDental.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class modulo
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public modulo()
        {
            this.operaciones = new HashSet<operaciones>();
        }
    
        public int id { get; set; }
        public string nombre { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<operaciones> operaciones { get; set; }
    }
}
