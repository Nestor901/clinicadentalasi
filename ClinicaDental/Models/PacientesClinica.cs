//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ClinicaDental.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class PacientesClinica
    {
        public int Id { get; set; }
        public int Genero_id { get; set; }
        public string Codigo { get; set; }
        [Required]
        public string Nombre { get; set; }
        [Required]
        public string Apellido { get; set; }
        [Required]
        public DateTime? FechaNacimiento { get; set; }
        public string Telefono { get; set; }
        public string Direccion { get; set; }
        public string CorreoElectronico { get; set; }
        public Nullable<System.DateTime> FechaPrimeraCita { get; set; }
        public string MotivoPrimeraCita { get; set; }
        public Nullable<double> Peso { get; set; }
        public string Pulso { get; set; }
        public Nullable<int> Temperatura { get; set; }
        public string PresionArterial { get; set; }
        public string NombreMedicoCabecera { get; set; }
        public string TelefonoMedico { get; set; }
        public string NombrePersonaReferencia { get; set; }
        public string NombreAlergia { get; set; }
        public string NombreMedicamento { get; set; }
        public string ObservacionesCirujano { get; set; }
    
        public virtual Genero Genero { get; set; }
    }
}
