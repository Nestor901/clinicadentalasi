﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ClinicaDental.Models;

namespace ClinicaDental.Controllers
{
    public class PacientesClinicasController : Controller
    {
        private ProyectoAsiEntities db = new ProyectoAsiEntities();

        // GET: PacientesClinicas
        public ActionResult Index()
        {
            var pacientesClinicas = db.PacientesClinicas.Include(p => p.Genero);
            return View(pacientesClinicas.ToList());
        }

        // GET: PacientesClinicas/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PacientesClinica pacientesClinica = db.PacientesClinicas.Find(id);
            if (pacientesClinica == null)
            {
                return HttpNotFound();
            }
            return View(pacientesClinica);
        }

        // GET: PacientesClinicas/Create
        public ActionResult Create()
        {
            ViewBag.Genero_id = new SelectList(db.Generoes, "Id", "Value");
            return View();
        }

        // POST: PacientesClinicas/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Genero_id,Codigo,Nombre,Apellido,FechaNacimiento,Telefono,Direccion,CorreoElectronico,FechaPrimeraCita,MotivoPrimeraCita,Peso,Pulso,Temperatura,PresionArterial,NombreMedicoCabecera,TelefonoMedico,NombrePersonaReferencia,Alergias,NombreAlergia,NombreMedicamento,ObservacionesCirujano")] PacientesClinica pacientesClinica)
        {
            if (ModelState.IsValid)
            {
                db.PacientesClinicas.Add(pacientesClinica);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Genero_id = new SelectList(db.Generoes, "Id", "Value", pacientesClinica.Genero_id);
            return View(pacientesClinica);
        }

        // GET: PacientesClinicas/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PacientesClinica pacientesClinica = db.PacientesClinicas.Find(id);
            if (pacientesClinica == null)
            {
                return HttpNotFound();
            }
            ViewBag.Genero_id = new SelectList(db.Generoes, "Id", "Value", pacientesClinica.Genero_id);
            return View(pacientesClinica);
        }

        // POST: PacientesClinicas/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Genero_id,Codigo,Nombre,Apellido,FechaNacimiento,Telefono,Direccion,CorreoElectronico,FechaPrimeraCita,MotivoPrimeraCita,Peso,Pulso,Temperatura,PresionArterial,NombreMedicoCabecera,TelefonoMedico,NombrePersonaReferencia,Alergias,NombreAlergia,NombreMedicamento,ObservacionesCirujano")] PacientesClinica pacientesClinica)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pacientesClinica).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Genero_id = new SelectList(db.Generoes, "Id", "Value", pacientesClinica.Genero_id);
            return View(pacientesClinica);
        }

        // GET: PacientesClinicas/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PacientesClinica pacientesClinica = db.PacientesClinicas.Find(id);
            if (pacientesClinica == null)
            {
                return HttpNotFound();
            }
            return View(pacientesClinica);
        }

        // POST: PacientesClinicas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PacientesClinica pacientesClinica = db.PacientesClinicas.Find(id);
            db.PacientesClinicas.Remove(pacientesClinica);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
