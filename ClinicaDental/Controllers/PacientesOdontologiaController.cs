﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ClinicaDental.Models;

namespace ClinicaDental.Controllers
{
    public class PacientesOdontologiaController : Controller
    {
        private ClinicaDentalEntities2 db = new ClinicaDentalEntities2();

        // GET: PacientesOdontologia
        public ActionResult Index()
        {
            var pacientesClinica = db.PacientesClinica.Include(p => p.Genero);
            return View(pacientesClinica.ToList());
        }

        // GET: PacientesOdontologia/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PacientesClinica pacientesClinica = db.PacientesClinica.Find(id);
            if (pacientesClinica == null)
            {
                return HttpNotFound();
            }
            return View(pacientesClinica);
        }

        // GET: PacientesOdontologia/Create
        public ActionResult Create()
        {
            ViewBag.Genero_id = new SelectList(db.Genero, "Id", "Value");
            return View();
        }

        // POST: PacientesOdontologia/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Genero_id,Codigo,Nombre,Apellido,FechaNacimiento,Telefono,Direccion,CorreoElectronico,FechaPrimeraCita,MotivoPrimeraCita,Peso,Pulso,Temperatura,PresionArterial,NombreMedicoCabecera,TelefonoMedico,NombrePersonaReferencia,NombreAlergia,NombreMedicamento,ObservacionesCirujano")] PacientesClinica pacientesClinica)
        {
            if (ModelState.IsValid)
            {
                db.PacientesClinica.Add(pacientesClinica);
                db.SaveChanges();
                pacientesClinica.Codigo = Getcodigo(pacientesClinica);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Genero_id = new SelectList(db.Genero, "Id", "Value", pacientesClinica.Genero_id);
            return View(pacientesClinica);
        }

        // GET: PacientesOdontologia/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PacientesClinica pacientesClinica = db.PacientesClinica.Find(id);
            if (pacientesClinica == null)
            {
                return HttpNotFound();
            }
            ViewBag.Genero_id = new SelectList(db.Genero, "Id", "Value", pacientesClinica.Genero_id);
            return View(pacientesClinica);
        }

        // POST: PacientesOdontologia/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Genero_id,Codigo,Nombre,Apellido,FechaNacimiento,Telefono,Direccion,CorreoElectronico,FechaPrimeraCita,MotivoPrimeraCita,Peso,Pulso,Temperatura,PresionArterial,NombreMedicoCabecera,TelefonoMedico,NombrePersonaReferencia,NombreAlergia,NombreMedicamento,ObservacionesCirujano")] PacientesClinica pacientesClinica)
        {
            if (ModelState.IsValid)
            {
                db.Entry(pacientesClinica).State = EntityState.Modified;
                db.SaveChanges();
                pacientesClinica.Codigo = Getcodigo(pacientesClinica);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Genero_id = new SelectList(db.Genero, "Id", "Value", pacientesClinica.Genero_id);
            return View(pacientesClinica);
        }

        // GET: PacientesOdontologia/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PacientesClinica pacientesClinica = db.PacientesClinica.Find(id);
            if (pacientesClinica == null)
            {
                return HttpNotFound();
            }
            return View(pacientesClinica);
        }

        // POST: PacientesOdontologia/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            PacientesClinica pacientesClinica = db.PacientesClinica.Find(id);
            db.PacientesClinica.Remove(pacientesClinica);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
        public string Getcodigo(PacientesClinica pacientesClinica)
        {
            PacientesClinica pacientes = new PacientesClinica();
            if (pacientesClinica.Id > 0 && pacientesClinica.Apellido != null && pacientesClinica.Apellido.Length > 0 && pacientesClinica.FechaNacimiento != null)
            {
                string[] apellidos = pacientesClinica.Apellido.Split(' ');
                string inicialApellidos = "";
                inicialApellidos += apellidos[0][0];
                if (apellidos.Count() > 1)
                    inicialApellidos += apellidos[1][0];
                else
                    inicialApellidos += apellidos[0][1];

                string codigo = String.Format("{0}{1}{2}", inicialApellidos.ToUpper(), pacientesClinica.FechaNacimiento.Value.ToString("yyyyMMdd"), pacientesClinica.Id);

                return codigo;
            }
            return null;
        }

    }
}
